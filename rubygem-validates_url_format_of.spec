# Generated from validates_url_format_of-0.3.0.gem by gem2rpm -*- rpm-spec -*-
%global gem_name validates_url_format_of
%global rubyabi 1.9.1

Summary: ActiveRecord URL Validation
Name: rubygem-%{gem_name}
Version: 0.3.0
Release: 1%{?dist}
Group: Development/Languages
License: MIT
URL: http://github.com/conickal/validates_url_format_of
Source0: http://rubygems.org/gems/%{gem_name}-%{version}.gem
Patch0: rubygem-validates_url_format_of-0.3.0-remove-simplecov.patch
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) >= 1.3.6
Requires: rubygem(activerecord) => 3.1.0
Requires: rubygem(activerecord) < 3.2
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: rubygems-devel >= 1.3.6
BuildRequires: rubygem(minitest)
BuildRequires: rubygem(rdiscount)
# SimpleCov is complex; patched out for now.
#BuildRequires: rubygem(simplecov)
BuildRequires: rubygem(sqlite3)
BuildRequires: rubygem(yard)
BuildRequires: rubygem(activerecord)
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
Rails plugin that provides a validates_url_format_of method to ActiveRecord
models. URLs are validated by regexp.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}

%prep
%setup -q -c -T
mkdir -p .%{gem_dir}
gem install --local --install-dir .%{gem_dir} \
            --force %{SOURCE0}

# Remove dependency on simplecov
pushd .%{gem_instdir}
%patch0 -p1
popd

# Remove developer files
rm -f .%{gem_instdir}/.gemtest
rm -f .%{gem_instdir}/.gitignore
rm -f .%{gem_instdir}/.travis.yml
rm -rf .%{gem_instdir}/.yardoc
# Remove extra gemspec file
rm -f .%{gem_instdir}/%{gem_name}.gemspec

%build

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%check
pushd .%{gem_instdir}
testrb -Ilib test/*_test.rb
popd


%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/LICENSE.md
%doc %{gem_instdir}/README.md
%{gem_instdir}/Gemfile
%{gem_instdir}/Rakefile
%{gem_instdir}/test


%changelog
* Sat Aug 04 2012 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.3.0-1
- Initial package, created by gem2rpm 0.8.1
